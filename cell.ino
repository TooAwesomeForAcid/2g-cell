SYSTEM_MODE(MANUAL);

char returnInfo[1] = "";		//Does this have to be a number? Is there some way to just not use it and pass a dead pointer?
String message = String("");

void setup()
{
  Serial.begin();
  Cellular.on(); 
}

void loop()
{	
	Cellular.command(callbackICCID, returnInfo, 60000, "AT+CGED=0\r\n");			
	delay(60000);
}

int callbackICCID(int type, const char* buf, int len, char* iccid)
{
	String chunk = String(buf);
	int endOfMessage = chunk.indexOf("OK");
	
	if (endOfMessage == -1)
	{
		message.concat(chunk);
	}
	
	else
	{
		message.concat(chunk.substring(0, endOfMessage + 2));
		message.replace("\r", "").replace("\n","");
		Serial.println(message);
				
		message = chunk.substring(endOfMessage + 3, chunk.length());
	}
	
	return WAIT;
}