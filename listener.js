const uuidv4 = require('uuid/v4');

var express = require('express')
var app = express()

app.get('/', function (req, res) {	
		res.send('TODO:  COMMAND LIST');
});

app.get('/api/GET_OVERVIEW', function (req, res) {
					
	conn.query(	"SELECT DISTINCT MCC, MNC, LAC, CI " +
				"FROM PINGS " +				
				"ORDER BY MCC, MNC, LAC, CI ASC",
				[],
				function (err, result) {							
					if (err) {console.log(err);}
					else { 
						res.header("Access-Control-Allow-Origin", "*");
						res.send(JSON.stringify(result));
					}
				})	
});

app.get('/api/GET_TOWER_SUMMARY', function (req, res) {
	
	var MCC = req.query.MCC;
	var MNC = req.query.MNC;
	var LAC = req.query.LAC;
	var CI = req.query.CI;
				
	conn.query(	"SELECT BSIC, ARFCN, MIN(RXLEV) AS MINPOWER, CAST(AVG(RXLEV) AS UNSIGNED) AS AVGPOWER, " +
				"MAX(RXLEV) AS MAXPOWER, CAST(MAX(RXLEV) / AVG(RXLEV) * 100 - 100 AS UNSIGNED) AS MAXDEVIATION, " +
				"MIN(TIME) FIRSTSEEN, MAX(TIME) AS LASTSEEN, COUNT(*) TIMESSEEN " +
				"FROM PINGS " +
				"WHERE MCC = ? AND MNC = ? AND LAC = ? AND CI = ? " +		
				"GROUP BY MCC, MNC, LAC, CI, BSIC, ARFCN " +
				"ORDER BY BSIC, ARFCN ASC",
				[MCC, MNC, LAC, CI],
				function (err, result) {							
					if (err) {console.log(err);}
					else { 					
						res.header("Access-Control-Allow-Origin", "*");
						res.send(JSON.stringify(result));
					}
				})	
});

app.get('/api/GET_TOWER_GRAPH', function (req, res){
	
	var MCC = req.query.MCC;
	var MNC = req.query.MNC;
	var LAC = req.query.LAC;
	var CI = req.query.CI;
	var BSIC = req.query.BSIC;
	var ARFCN = req.query.ARFCN;
	
	conn.query(	"SELECT RXLEV, TIME " +
				"FROM PINGS " +
				"WHERE MCC = ? AND MNC = ? AND LAC = ? AND CI = ? AND BSIC = ? AND ARFCN = ? " +
				"ORDER BY TIME DESC",
				[MCC, MNC, LAC, CI, BSIC, ARFCN],
				function (err, result) {							
					if (err) {console.log(err);}
					else { 
					
						var returnString = "[ ";						
						
						for(var i = 0; i < result.length; i++)
						{
								returnString += ("[" + new Date(result[i].TIME).getTime() + "," + result[i].RXLEV + "]");
								
								if (i < (result.length - 1))
								{
									returnString += ',';
								}
						}
						
						returnString += " ]";
						
						res.header("Access-Control-Allow-Origin", "*");
						res.send(returnString);
					}
				})	
	
});

app.listen(6699);

var mySQL = require('mysql');
var conn = mySQL.createConnection({
		host: "localhost",
		user: "anonymous",
		password: "",	
		database: "CELL"
});

conn.connect(function (err) { if (err) { console.log(err); } });

process.on('SIGINT', function () { process.exit(); });
process.on('uncaughtException', function (err) { process.exit();});
process.on('exit', ExitHandler);

function ExitHandler (options, err)
{
	conn.end();
}