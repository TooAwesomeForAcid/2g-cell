The process so far:

1)  cell.ino - This is the code that is flashed onto the electron itself. It sends the AT commands to the GSM radio and gathers the response.
	Currently it then passes the response over the USB port to serialrelay.js. I would like to investigate using the GSM radio to connect to wifi 
	(this is apprently possible with AT commands, but not implemented by particle?) as well as gather GPS data (definitely possible with another AT command)
	and send data home using an HTTP request to listener.js.
	
2)  serialrelay.js - I would like to more or less do away with this part entirely, but we're using it right now to gather the data from the electron, parse it using regex, 
	and insert it into the mysql database. Eventually I would like for all of the transmission to be done on the electron and the parsing and insertion to be done by the listener.js app.
	
3) 	listener.js - This currently just listens for requests on a given port and serves out db data to run the website. I would like for it to eventually handle parsing and inserting new ping data.

4)	cell.html - The website for visualizing the cell phone data.

Some resources:

https://www.u-blox.com/sites/default/files/u-blox-CEL_ATCommands_%28UBX-13002752%29.pdf -- AT Commands for the SARA GSM radio

https://docs.particle.io/reference/firmware/electron/ -- Particle Electron reference guide

https://docs.particle.io/reference/cli/#particle-flash -- How to use the CLI to compile / flash code to the electron (you have to do this since you won't be using a SIM to do OTA)

Feel free to make any changes you want!