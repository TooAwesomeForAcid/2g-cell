var SerialPort = require('serialport');

const uuidv4 = require('uuid/v4');

var mySQL = require('mysql');
var conn = mySQL.createConnection({
		host: "localhost",
		user: "anonymous",
		password: "",	
		database: "CELL"
});

conn.connect(function (err) { if (err) { console.log(err); } });

process.on('SIGINT', function () { process.exit(); });
process.on('uncaughtException', function (err) { process.exit();});
process.on('exit', ExitHandler);

var port;

process.stdout.write('Searching for attached k-radio...');

SerialPort.list(function (err, list) {
	if (err)
	{		
		console.log('\n\n' + err);
	}
	
	else
	{
		var portPath = list.find(function (element) { return element.manufacturer == 'Particle';}).comName; 
		
		if (portPath)
		{
			process.stdout.write('found on ' + portPath + '!\n');
		}
		
		else
		{
			process.stdout.write('not found...is it plugged in?\n\n');
			process.exit();
		}
		
		process.stdout.write('Connecting...');
		
		port = new SerialPort(portPath, function (err) {	
			if (err)
			{
				console.log('\n\n' + err);
			}
			
			else
			{
				process.stdout.write('Success!\n');
				console.log('Surveying local cell network...');
			}
		});

		var message = "";

		port.on('readable', function () {
			
			message += port.read().toString();		
				
			if (message.indexOf('OK') != -1)
			{
				
				
				
				message = message.replace(/\n|\r|\s/g,"")								//Whitespace
								 .replace(/AT\+CGED\=0/g,"")							//Command name all the fucking time christ
								 .replace(/\WEquivalent(\S*?)\W(?=Arfcn)/g, ",")		//Equivalent PLMNS data
								 .replace(/GPRS(\S*?)(?=OK)/, '');						//GPRS settings data
				
				console.log(message);				
				
				var matched = message.match(/(MCC:|CI:)(\S*?)(?=Neighbour|OK)/g);
				
				console.log(matched);


							
				var surveyInfo = [];
				
				//var matched = message.match(/(?:MCC:)(\w*), MNC:(\w*), LAC:(\w*), CI:(\w*), BSIC:(\w*), ARFCN:(\w*), RXLEV:(\w*),/gi);
							
				
				for (var i = 0; i < matched.length; i++)
				{
					var towerInfo = {};			
					var splitMatch = matched[i].split(',');
							
					for (var x = 0; x < splitMatch.length; x++)
					{
							var splitPair = splitMatch[x].split(':');
							towerInfo[splitPair[0]] = splitPair[1];
					}
					
					surveyInfo.push(towerInfo);			
				}
								
				console.log('Writing ' + surveyInfo.length + ' towers to db.');
								
				for (var i = 0; i < surveyInfo.length; i++)
				{
					conn.query("INSERT INTO PINGS (PINGID, IP, MCC, MNC, LAC, CI, BSIC, ARFCN, RXLEV, TIME) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())",
								[uuidv4(), '162.193.21.110', surveyInfo[i].MCC, surveyInfo[i].MNC, surveyInfo[i].LAC, surveyInfo[i].CI, surveyInfo[i].BSIC, surveyInfo[i].Arfcn, surveyInfo[i].RxLev || surveyInfo[i].RxLevServ],
								function (err, result) {							
									if (err) {console.log(err);}
									//else { console.log("Affected: " + result.affectedRows); }
								})
				}
			
				message = "";		
			}	
		});
	}	
});

function ExitHandler (options, err)
{		
	port.close();	
	conn.end();
}